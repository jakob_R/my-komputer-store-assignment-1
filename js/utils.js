export function createAlert(type, alertMsg) {
  const elAlert = document.createElement('div');
  elAlert.role = 'alert';
  if (type == 'danger') {
    elAlert.className = 'alert alert-dismissible alert-danger';
  } else if (type == 'success') {
    elAlert.className = 'alert alert-dismissible alert-success';
  } else {
    elAlert.className = 'alert alert-dismissible alert-info';
  }
  elAlert.innerHTML =
    alertMsg +
    `<button type="button" class="btn-close" 
            data-bs-dismiss="alert" aria-label="Close">
            </button>`;
  return elAlert;
}
