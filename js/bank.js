import { createAlert } from './utils.js';

export class Bank {
  // HTML Elements
  elBalance = document.getElementById('balance');
  elGetLoan = document.getElementById('get-loan');
  elOutstandingLoan = document.getElementById('outstanding-loan');
  elLoanAmount = document.getElementById('loan-amount');
  elBankCard = document.getElementById('bank-card');
  elBankAlert = document.getElementById('bank-alert');
  elPayLoan = document.getElementById('pay-loan');
  // Properties
  balance = 200;
  outStandingLoan = 0;
  hasLoan = false;
  mustBuyComputer = false;
  // Status messages
  invalidLoanInput = 'Amount must be a number above 1';
  loanMoreThanDouble = 'Loan cannot be more than double your balance';
  alreadyHasLoan = 'You must pay back your previous loan';
  mustBuyComputerMsg =
    'You cannot get more than one bank loan before buying a computer';
  loanRecieved = 'Loan received successfully';

  async init() {
    this.createEventListeners();
  }

  createEventListeners() {
    // Event Listeners
    this.elGetLoan.addEventListener('click', this.onGetLoan.bind(this));
  }

  onGetLoan() {
    //Checks if user already has loan
    if (this.hasLoan) {
      this.elBankCard.appendChild(createAlert('danger', this.alreadyHasLoan));
      return;
    }
    //Checks if has taken a previous loan without buying a computer
    if (this.mustBuyComputer) {
      this.elBankCard.appendChild(
        createAlert('danger', this.mustBuyComputerMsg)
      );
      return;
    }
    let amount = parseFloat(prompt('Please enter amount', '0'));
    if (this.validateLoan(amount)) {
      this.updateLoan(amount);
      this.balance = this.balance + amount;
      this.updateElBalance();
      this.mustBuyComputer = true;

      this.elBankCard.appendChild(createAlert('success', this.loanRecieved));
    }
  }

  validateLoan(amount) {
    //Checks if input is a number above 1
    if (!Number.isFinite(amount) || !(amount > 1)) {
      this.elBankCard.appendChild(createAlert('danger', this.invalidLoanInput));
      return false;
    }
    //Checks if amount is more than Double
    if (amount > 2 * this.balance) {
      this.elBankCard.appendChild(
        createAlert('danger', this.loanMoreThanDouble)
      );
      return false;
    }
    return true;
  }

  updateLoan(amount) {
    //adds amount to outstanding loan
    this.outStandingLoan = this.outStandingLoan + amount;
    //updates hasLoan
    this.outStandingLoan == 0 ? (this.hasLoan = false) : (this.hasLoan = true);
    //Updates html elements based on hasloan
    if (this.hasLoan) {
      this.elLoanAmount.innerHTML = this.outStandingLoan;
      this.elOutstandingLoan.style.display = 'block';
      this.elPayLoan.style.display = 'inline-block';
    } else {
      this.elLoanAmount.innerHTML = null;
      this.elOutstandingLoan.style.display = 'none';
      this.elPayLoan.style.display = 'none';
    }
  }

  updateElBalance() {
    this.elBalance.innerHTML = this.balance;
  }

  transferBalance(val) {
    this.balance = this.balance + val;
    this.updateElBalance();
  }

  buyComputer(val) {
    this.transferBalance(val);
    this.mustBuyComputer = false;
  }

  render() {}
}
