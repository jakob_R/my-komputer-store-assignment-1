import { fetchComputers } from './api.js';
import { ComputerInfoView } from './computer-info.js';

export class AppComputer {
  // HTML Elements
  elComputerSelect = document.getElementById('computer-select');
  elComputerFeatures = document.getElementById('computer-features');
  // Data properties
  computers = [];
  selectedComputer = null;
  computerInfoView = new ComputerInfoView();
  bank;

  // Status properties
  error = '';

  constructor(bank) {
    this.bank = bank;
  }

  async init() {
    await this.createComputers();
    this.createEventListeners();
  }

  async createComputers() {
    try {
      this.computers = await fetchComputers().then((computers) =>
        computers.map((computer) => new Computer(computer))
      );
    } catch (e) {
      this.error = e.message;
      console.log(e);
    }
  }

  createEventListeners() {
    // Event Listeners
    this.elComputerSelect.addEventListener(
      'change',
      this.onComputerChange.bind(this)
    );
  }

  onComputerChange() {
    if (parseInt(this.elComputerSelect.value) === -1) {
      // Reset the current computer.
      return;
    }

    const selectedComputer = this.computers.find((computer) => {
      return computer.id == this.elComputerSelect.value;
    });

    this.elComputerFeatures.innerHTML = '';
    //Create feature list
    let list = document.createElement('ul');
    for (let i = 0; i < selectedComputer.features.length; i++) {
      let item = document.createElement('li');
      item.appendChild(document.createTextNode(selectedComputer.features[i]));
      list.appendChild(item);
    }

    this.elComputerFeatures.appendChild(list);

    this.computerInfoView.setComputer(selectedComputer, this.bank);
  }

  createDefaultOptionForSelect() {
    const elComputer = document.createElement('option');
    elComputer.innerText = '-- Select a computer --';
    elComputer.value = -1;
    this.elComputerSelect.appendChild(elComputer);
  }

  render() {
    this.createDefaultOptionForSelect();

    this.computers.forEach((computer) => {
      this.elComputerSelect.appendChild(computer.createComputerSelectOption());
    });

    this.elComputerSelect.disabled = false;
  }
}

export class Computer {
  constructor(computer) {
    this.id = computer.id;
    this.name = computer.name;
    this.price = computer.price;
    this.description = computer.description;
    this.features = computer.features;
    this.image = computer.image;
    this.rating = computer.rating;
    this.specs = computer.specs;
  }

  createComputerSelectOption() {
    const elComputerOption = document.createElement('option');
    elComputerOption.value = this.id;
    elComputerOption.innerText = this.name;
    return elComputerOption;
  }
}
