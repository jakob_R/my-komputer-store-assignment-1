import { Bank } from './bank.js';
import { createAlert } from './utils.js';

export class Work {
  //Html elements
  elWorkCard = document.getElementById('work-card');
  elBankTransfer = document.getElementById('bank-transfer');
  elDoWork = document.getElementById('do-work');
  elPayLoan = document.getElementById('pay-loan');
  elPay = document.getElementById('pay');
  elOutstandingLoan = document.getElementById('outstanding-loan');

  // Properties
  bank;
  pay = 0;

  // Status messages
  noPay = 'You have no pay, to pay your loan';
  nothingToTransfer = 'You have no pay to transfer';

  constructor(bank) {
    this.bank = bank;
  }

  async init() {
    this.createEventListeners();
  }

  createEventListeners() {
    // Event Listeners
    this.elDoWork.addEventListener('click', this.onDoWork.bind(this));
    this.elBankTransfer.addEventListener(
      'click',
      this.onBankTransfer.bind(this)
    );
    this.elPayLoan.addEventListener('click', this.onPayLoan.bind(this));
  }

  onPayLoan() {
    //Check if there is any pay to pay down loan with or display alert
    if (this.pay > 0) {
      //check if pay value is lower than the loan.
      if (this.pay <= this.bank.outStandingLoan) {
        this.bank.updateLoan(-this.pay);
        this.pay = 0;
        this.elPay.innerHTML = this.pay;
      } else if (this.pay > this.bank.outStandingLoan) {
        let loan = this.bank.outStandingLoan;
        this.bank.updateLoan(-loan);
        this.pay = this.pay - loan;
        this.elPay.innerHTML = this.pay;
      }
    } else {
      this.elWorkCard.appendChild(createAlert('danger', this.noPay));
    }
  }

  onPayDeduction(pay) {
    this.bank.updateLoan(-pay);
  }

  onDoWork() {
    //Add 100 to pay and display
    this.pay = this.pay + 100;
    this.elPay.innerHTML = this.pay;
  }

  onBankTransfer() {
    //Display alert if there is no pay to transfer
    if (this.pay <= 0) {
      this.elWorkCard.appendChild(
        createAlert('danger', this.nothingToTransfer)
      );
      return;
    }
    //Deduct 10% to outstanding loan
    if (this.bank.hasLoan) {
      let deduction = this.pay * 0.1;
      this.pay = this.pay - deduction;
      this.onPayDeduction(deduction);
    }
    //Transfer pay to bank balance
    this.bank.transferBalance(this.pay);
    this.pay = 0;
    this.elPay.innerHTML = this.pay;
  }

  render() {}
}
