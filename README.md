# My Komputer Store

 My Komputer Store is a single page website consisting og 3 main parts. 
 A bank where you can view your balance and take out a loan
 Work where you can earn money by working, transfer to the bank and pay down your loan.  
 Laptops section where you can select a computer, view info and buy computers. 

## Usage 

To use the store the json server must be running.

Run frontend app using liveserver

## About 

The laptops are stored using a json server in the json-server-with-node folder from the shared gitlab repo.

The frontend consist of a single index.html file and the javascript files inside the js folder.

Each part of the store roughly corresponds to a js file which manages their functionality. There is a bank,work ,computer and ComputerInfoView class which are instantiated in index.js. work, computer and ComputerInfoView all have access to the same bank instance. There is a api.js file for the api calls and a utils file with a helper function. 

For Design and layout i have used bootstrap. 


