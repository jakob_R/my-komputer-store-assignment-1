import { createAlert } from './utils.js';

export class ComputerInfoView {
  //HTML elements
  elComputerInformation = document.getElementById('card-computer-info');
  // Data properties
  computer;
  bank;

  setComputer(computer, bank) {
    this.computer = computer;
    this.bank = bank;
    this.render();
  }

  buyComputer() {
    //display alert if bank balance is to low
    if (this.bank.balance < this.computer.price) {
      this.elComputerInformation.appendChild(
        createAlert('danger', 'Your balance is too low')
      );
      return;
    }
    //Buy pc and display message
    this.bank.buyComputer(-this.computer.price);
    this.elComputerInformation.appendChild(
      createAlert('success', 'Computer bought successfully')
    );
  }

  render() {
    //Reset selectedComputerInformation
    this.elComputerInformation.innerHTML = '';

    //Create  Computer image column
    const elComputerImgContainer = document.createElement('div');
    elComputerImgContainer.className = 'col-md';
    const image = new Image();
    image.className = 'img-left';
    image.width = 320;
    image.onload = () => {
      elComputerImgContainer.appendChild(image);
    };
    image.src = this.computer.image;
    this.elComputerInformation.appendChild(elComputerImgContainer);

    //Create  computer info column
    const elComputerInfoContainer = document.createElement('div');
    elComputerInfoContainer.className = 'col-md mx-3';

    const elComputerName = document.createElement('h3');
    elComputerName.classList.add('card-title');
    elComputerName.innerText = this.computer.name;
    elComputerInfoContainer.appendChild(elComputerName);

    const elComputerDescription = document.createElement('p');
    elComputerDescription.classList.add('card-text');
    elComputerDescription.innerText = this.computer.description;
    elComputerInfoContainer.appendChild(elComputerDescription);

    const elComputerRating = document.createElement('span');
    elComputerRating.innerText = `Rating: ${this.computer.rating}/10`;
    elComputerInfoContainer.appendChild(elComputerRating);

    const elComputerPrice = document.createElement('h5');
    elComputerPrice.innerText = `NOK ${this.computer.price}`;
    elComputerInfoContainer.appendChild(elComputerPrice);

    const elComputerBtn = document.createElement('a');
    elComputerBtn.className = 'btn btn-primary mb-3';
    elComputerBtn.id = 'buy-selected-computer';
    elComputerBtn.innerText = 'Buy Now';
    elComputerBtn.addEventListener('click', this.buyComputer.bind(this));
    elComputerInfoContainer.appendChild(elComputerBtn);

    this.elComputerInformation.appendChild(elComputerInfoContainer);
  }
}
