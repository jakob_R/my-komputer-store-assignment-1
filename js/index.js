import { AppComputer } from './computer.js';
import { Bank } from './bank.js';
import { Work } from './work.js';

class App {
  constructor() {
    this.bank = new Bank();
    this.work = new Work(this.bank);
    this.computer = new AppComputer(this.bank);
  }

  async init() {
    await this.bank.init();
    await this.work.init();
    await this.computer.init();
    this.render();
  }

  render() {
    this.computer.render();
  }
}

new App().init();
